I have a simple layout comprised of a `Row` with:

1) Red `Box` on the left that should fill the height and have the same width (square)
2) `Column` with a couple of `Text`s on the right

The problem is that when I add enough `Text`s to the `Column`, the last one is not rendered anymore. The `IntrinsicSize.Min` seems to be the issue. And in a few (hard to reproduce) cases I saw this behavior even when the red `Box` had fixed size (no aspect ratio).

For the `Composable` in [MainActivity.kt](https://gitlab.com/pesathem/compose-intrinsic-height-bug/-/blob/master/app/src/main/java/com/example/intrinsicheight/MainActivity.kt), this is the result (last `Text` with "Android" is cut off):

![Bug screenshot](intrinsic.png)
