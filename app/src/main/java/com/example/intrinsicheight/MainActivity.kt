package com.example.intrinsicheight

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ComposeCompilerApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.intrinsicheight.ui.theme.IntrinsicHeightTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            IntrinsicHeightTheme {
                Box(
                    Modifier
                        .fillMaxSize()
                        .background(Color.LightGray)) {
                    IntrinsicBug()
                }
            }
        }
    }

    @Preview
    @Composable
    fun IntrinsicBug() {
        Row(
            Modifier
                .background(Color.Green)
                .height(IntrinsicSize.Min)
        ) {
            Box(
                Modifier
                    .background(Color.Red)
                    .fillMaxHeight()
                    .aspectRatio(1f)
            )
            Column(
                Modifier
                    .weight(1f)
                    .padding(12.dp)
            ) {
                Text("Hello Hello Hello Hello Hello Hello Hello Hello")
                Text("Welcome")
                Text("Bye")
                Text("Foo")
                Text("Bar")
                Text("Google")
                Text("Android")
            }
        }
    }
}
